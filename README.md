# Installation

First, install [poetry](https://github.com/sdispater/poetry/).

`pip install --user poetry`

Then clone this repository and execute

`poetry install`

# How to run the app

`poetry run python -m gc_log_watcher <log filename>`

# Example log

`poetry run python -m gc_log_watcher test.log`
