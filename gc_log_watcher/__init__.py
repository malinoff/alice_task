import re
from datetime import datetime, timedelta

import pygtail


def process_log(filename):
    """Main entry point of the script.

    filename is the name of the file to parse log events from.
    """
    # A 5-minute window of "Full GC" events
    time_window = 5 * 60
    full_gc_events = []
    # pygtail automatically handles file rotation
    for line in pygtail.Pygtail(filename):
        # In general, events processing is split into three parts:
        # 1. parsing log file lines,
        type_, timestamp, data = parse_line(line.strip('\n'))
        # 2. sending all events to newrelic,
        send_to_newrelic(type_, timestamp, data)
        # 3. maintaining a 5-minute window of "Full GC" events
        #    and handling the restart logic.
        if type_ == 'full_gc':
            full_gc_events.append((timestamp, data))
            maybe_restart_tomcat(full_gc_events, time_window)
            if timestamp - full_gc_events[0][0] >= timedelta(minutes=5):
                full_gc_events.pop(0)  # Remove the oldest event

                
def send_to_newrelic(type_, timestamp, data):
    """Dummy function to show what data will be sent to newrelic."""
    #newrelic.agent.record_custom_event(
    #    event_type=type_,
    #    params={'log_timestamp': str(timestamp), **data},
    #)
    print('Sending to new relic: {}'.format({
        'event_type': type_,
        'params':{'log_timestamp': str(timestamp), **data},
    }))


def maybe_restart_tomcat(events, time_window):
    """If conditions are met, restart tomcat."""
    freed_memory_low = count_freed_memory_low(events) > 10
    full_gc_takes_lots_of_time = total_gc_duration(events) / time_window > 0.15
    if freed_memory_low or full_gc_takes_lots_of_time:
        if other_nodes_responsive():
            restart_tomcat()


def other_nodes_responsive():
    """Dummy function that should really check other nodes responsiveness."""
    print('Checking other nodes')
    return True


def restart_tomcat():
    """Dummy function that should really restart tomcat."""
    print('Restarting tomcat')


def count_freed_memory_low(events):
    result = 0
    for timestamp, data in events:
        if data['memory_after'] / data['memory_before'] >= 0.8:
            # 80% of memory was left intact, or 20% of memory was freed
            result += 1
    return result


def total_gc_duration(events):
    duration = 0
    for timestamp, data in events:
        duration += data['duration']
    return duration


class ParseError(Exception):
    """Raises when parsing fails in any way."""


def parse_line(line):
    """Parse a single line of GC log."""
    # Parse the line into "left", which is the timestamp,
    # and "right", which is the payload to be parsed further.
    left, right = line.split('[', 1)
    timestamp = left.split(': ')[0]
    timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%f%z')

    right = right.rstrip(']')
    try:
        if right.startswith('Full GC'):
            data = _parse_full_gc(right)
            type_ = 'full_gc'
        elif right.startswith('GC pause'):
            data = _parse_gc_pause(right)
            type_ = 'gc_pause'
        else:
            data = _parse_gc_event(right)
            type_ = 'gc_event'
    except ParseError:
        raise ParseError('unsupported line format: {!r}'.format(line))
    except Exception:
        raise ParseError('could not parse line: {!r}'.format(line))
    
    return type_, timestamp, data


FULL_GC_RE = re.compile(
    r'Full GC \((?P<cause>.+)\) (?P<memory>.+)\(.*\), (?P<duration>.+) secs'
)


def _parse_full_gc(line):
    match = FULL_GC_RE.match(line)
    if match is None:
        raise ParseError
    parsed = match.groupdict()
    memory_before, memory_after = map(
        _parse_memory_unit,
        parsed['memory'].split('->'),
    )
    return {
        'cause': parsed['cause'],
        'memory_before': memory_before,
        'memory_after': memory_after,
        'duration': float(parsed['duration']),
    }


def _parse_memory_unit(unit):
    """Helper to convert values in kylobytes or megabytes to bytes."""
    value, unit = int(unit[:-1]), unit[-1]
    if unit == 'K':
        return value * 1024
    elif unit == 'M':
        return value * 1024 * 1024


GC_PAUSE_RE = re.compile(
    r'GC pause \((?P<action>.+)\) \((?P<collector>.+)\)( \((?P<cause>.+)\))?, (?P<duration>.+) secs'
)


def _parse_gc_pause(line):
    match = GC_PAUSE_RE.match(line)
    if match is None:
        raise ParseError
    parsed = match.groupdict()
    return {
        'action': parsed['action'],
        'collector': parsed['collector'],
        'cause': parsed['cause'],
        'duration': float(parsed['duration']),
    }


GC_EVENT_RE = re.compile(r'GC (?P<event>[\w-]+).*, (?P<duration>.+) secs')


def _parse_gc_event(line):
    match = GC_EVENT_RE.match(line)
    if match is None:
        raise ParseError
    parsed = match.groupdict()
    return {
        'event': parsed['event'],
        'duration': float(parsed['duration']),
    }
