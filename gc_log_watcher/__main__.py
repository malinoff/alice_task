import argparse

from gc_log_watcher import process_log

parser = argparse.ArgumentParser('gc_log_watcher')
parser.add_argument('log', help='Log filename to parse events from')
args = parser.parse_args()
process_log(args.log)
